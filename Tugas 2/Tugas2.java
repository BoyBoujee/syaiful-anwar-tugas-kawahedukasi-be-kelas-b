public class Tugas2 {
    public static void main(String[] args) {

        //ini line buat manggil method nilai absolut
        nilaiAbsolut(-3); //ini keyword methodnya (parameter)
        nilaiAbsolut(5); //ini keyword methodnya (parameter)
        //line method nilai absolut end
 
        //ini line buat manggil angka terbesar 
        int[] arr = {1, 2, 3, 4}; //{ini parameternya}
        angkaTerbesar(arr); //ini keyword methodnya
        int[] arr2 = {1, 2, 3, 8}; //{ini parameternya}
        angkaTerbesar(arr2); //ini keyword methodnya
        //line method angka terbesar end

        //ini line buat manggil pangkat
        int x = 2; //ini parameter angka yg akan dipangkatkan
        int y = 3; //ini parameter pangkatnya
        pangkat(x, y); //ini keyword methodnya
        //line method pangkat end
    }   
        //method buat cari nilai absolut
        public static void nilaiAbsolut(int a) {
            if(a < 0) {
                System.out.println("nilai absolut dari angka " + a + " adalah " + a * -1);
            }
            else{
                System.out.println("nilai absolut dari angka " + a + " adalah " + a);
            }                     
        }
        //method cari nilai absolut end

        //method cari angka terbesar
        public static void angkaTerbesar(int[] arr) {
            int angkaTerbesar = arr[0];
            for(int i = 0; i < arr.length; i++) 
            {
                if(arr[i] >angkaTerbesar)
                {
                    angkaTerbesar = arr[i];
                }
             }
             System.out.println("angka terbesar adalah " + angkaTerbesar);
        }
        //method angka terbesar end
        
        //method pangkat
        public static void pangkat(int x, int y) {
            int i,j,temp1=0,temp2=x,hasil=0;
            for (i=1; i<=y; i++){
            for (j=1; j<=(x-1); j++){
            temp2=temp2+temp1;
            }
            temp1=temp2;
            }
            hasil=temp2;
            System.out.println(x + " pangkat " + y + " adalah " + hasil);
            }
            }
        //method pangkat end
        
     
    
