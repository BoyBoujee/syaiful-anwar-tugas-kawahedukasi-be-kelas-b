import java.util.Scanner;

public class Tugas1 {
    public static int MenuData()
    {
        int selection;
        Scanner sc = new Scanner (System.in);
        System.out.println("Select your option");
        System.out.println("------------------");
        System.out.println("[1] Identitas   ");
        System.out.println("[2] Kalkulator  ");
        System.out.println("[3] Perbandingan");
        System.out.println("------------------");

        System.out.println("Your selected option is: ");
        selection = sc.nextInt();
        sc.close();
        return selection;
    }
    
    public static void main(String[] args) {
        int userSelected;
        do{
            userSelected = MenuData();
            switch(userSelected) {
            case 1:
                //ini code identitas
                System.out.println("Option 1 is selected");
                System.out.println("------------------------------------------------------------------------------------");
                System.out.println("Nama: Syaiful Anwar");
                System.out.println("------------------------------------------------------------------------------------");
                System.out.println("Alasan saya menjadi seorang back-end developer:    back-end developer adalah pekerjaan yang menjanjikan baik secara benefit serta jenjang karir.");
                System.out.println("------------------------------------------------------------------------------------");
                System.out.println("Ekspektasi saya dengan mengikuti bootcamp ini:     saya memiliki keahlian untuk merancang atau mengembangkan software di sisi server yang berkaitan dengan logika serta database.");
                System.out.println("------------------------------------------------------------------------------------");
                break;
            //code identitas end

            case 2:

            //ini code kalkulator
                System.out.println("Option 2 is selected");
                Scanner reader = new Scanner(System.in);
                System.out.println("Enter num1: ");
                int num1 = reader.nextInt();

                System.out.println("Enter num2: ");
                int num2 = reader.nextInt();

                System.out.println("Enter operator (+,-,*,/): ");
                int operator = reader.next().charAt(0);

                int result;
                //code kalkulator end

                //ini 4 pilihan operasi perhitungan (+,-,*,/)
                switch(operator) {
                    case '+':
                        result = num1 + num2;
                        break;
                    case '-':
                        result = num1 - num2;
                        break;
                    case '*':
                        result = num1 * num2;
                        break;
                    case '/':
                        result = num1 / num2;
                        break;
                    default:
                        System.out.println("Operator is incorrect!");
                        return;
                 }
                //opsi perhitungan end

                System.out.println(num1 + " " + operator + " " + num2 + " = " + result);
                // code kalkulator end

                break;
            case 3:
                System.out.println("Option 3 is selected");
                
                //ini buat user input angka yg mau dibandingin
                Scanner readerC = new Scanner(System.in);
                System.out.println("Enter num1: ");
                int num1C = readerC.nextInt();

                System.out.println("Enter num2: ");
                int num2C = readerC.nextInt();

                //input user end

                //ini kode outputnya
                if(num1C>num2C){
                    System.out.println(num1C+" is greater than "+num2C);}
                else if(num1C<num2C){
                    System.out.println(num2C+" is greater than "+num1C);}
                else{
                    System.out.println(num1C+" is equal to "+num2C);}
                break;
                //kode output end
            default:
                break;
            }
        }
        while(userSelected > 3);
    }    
}